<?php

// $Id: Social Score Facebook dev  $

/**
 * @file
 */

/**
 * Implementation of hook_form().
 */
function socialscore_facebook_form(&$form) {

  $form = array();

  $form['api_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook url API'),
    '#default_value' => variable_get('facebook_score_api_url', 'http://api.facebook.com/restserver.php?method=links.getStats&urls='),
    '#required' => TRUE,
    '#description' => t('The Facebook URL API to send xml requests'),
  );
  
  $form['attributes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Which features to count'),
    '#options' => array(
      'fb_likeCount' => 'Count FB Likes',
      'fb_commentCount' => 'Count FB Comments',
      'fb_shareCount' => 'Count FB Share',
      'fb_clickCount' => 'Count FB Clicks',
    ),
    '#default_value' => variable_get('facebook_score_active_features' , array('')),
    '#description' => t('Enable the features, you would like to count from Facebook API.'),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );
  
  return $form;
}

/**
 * Implementation of hook_form_alter.
 */
function socialscore_facebook_form_alter(&$form, &$form_state, $form_id) {
  social_score_clear_views_cache();
}


/**
 * Implementation of hook_form_validate().
 */
function socialscore_facebook_form_validate(&$form, $form_values) {
  if (!valid_url($form_values['values']['api_field'], TRUE)) {
    form_set_error('api_field' , t('This isn\'t a valid url. Please try again.'));
  }
}

/**
 * Submit handler for the facebook admin settings form.
 */
function socialscore_facebook_form_submit(&$form, $form_values) {
  variable_set('facebook_score_active_features', $form_values['values']['attributes']);
  variable_set('facebook_score_api_url', $form_values['values']['api_field']);
  drupal_set_message(t('Your settings have been saved.'));
}
