<?php

// $Id: Social Score  $

/**
 * @file
 *   social_score.views.inc
 */

function social_score_views_data() {

  $data['social_score']['table']['group'] = t('Social Score');

  $data['social_score']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  // Social score field.
  $data['social_score']['social_score'] = array(
    'title' => t('Social Score'),
    'help' => t('A sum of all active social networks score feeds.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $info = social_score_available_data_for_views_fields();
  
  foreach ($info as $active_field) {
    //available fields
    if (social_score_is_enabled_field($active_field['database_field'])) {
      $data['social_score'][$active_field['database_field']] = array(
        'title' => $active_field['title'],
        'help' => $active_field['help'],
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      );
    }
  }
  return $data;
}

/**
 * This function returns an array which includes 
 * information to be shown in the views UI about social score sub modules fields.
 *
 * @return
 *   Return array
 */
function social_score_available_data_for_views_fields() {
  $sub_modules_list = array_keys(social_score_social_score_core_info());
  $views_fields = array();
  foreach ($sub_modules_list as $module) {
    $function = $module . '_get_info_for_views_fields';
    $function($views_fields);
  }
  return $views_fields;
}