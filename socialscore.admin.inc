<?php

// $Id: Social Score dev  $

/**
 * @file
 */


/**
 * Admin settings for social_score.
 */
function socialscore_form(&$form) {

  $form = array();

  //Checkboxes with available content types
  $form['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#options' => node_get_types('names'),
    '#description' => t('Select the content types, which you want to enable social score.'),
    '#default_value' => variable_get('content_types', array()),
  );

  //Seconds for url's statistics expiration
  $form['seconds'] = array(
    '#type' => 'textfield',
    '#title' => t('How often do you want to synchronize your content'),
    '#maxlength' => 4,
    '#size' => 4,
    '#default_value' => variable_get('seconds', '60'),
    '#description' => t('Time is in seconds.'),
  );

  //If the path module is enabled, show this
  if (module_exists('path')) {
    $form['alias'] = array(
      '#type' => 'radios',
      '#title' => t('Which url do you want to use'),
      '#default_value' => variable_get('alias', 'noalias'),
      '#options' => array(
        'noalias' => t('Synchronize the node via drupal\'s default url, for example (node/nid)'),
        'alias' => t('Synchronize the node via url alias, for example (/about)'),
        'both' => t('Synchronize the node via both default url and alias, we sum the score of each url'),
      ),
      '#description' => t('By default is Drupal\'s default url.')
    );
  }

  //Cron select sql limit
  $form['cron_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('SQL limit for cron sychronization'),
    '#maxlength' => 4,
    '#size' => 4,
    '#default_value' => variable_get('cron_limit', '1000'),
    '#description' => t('This number describes the number of node\'s sychonization per "cron run". It is used for SELECT (LIMIT) query.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}



/**
 * Submit handler for the admin settings form.
 */
function socialscore_form_submit(&$form, $form_values) {
  variable_set('alias', $form_values['values']['alias']);
  variable_set('seconds', $form_values['values']['seconds']);
  variable_set('content_types', $form_values['values']['content_types']);
  variable_set('cron_limit', $form_values['values']['cron_limit']);
  drupal_set_message(t('Your settings have been saved...'));
  if (module_exists('path')) drupal_set_message(t('If you had made changes at content type\'s checkboxes settings, or at sychronization url category, you should run the ' . l(t('batch'), 'admin/settings/social-score/synchronization') . ' to synchronize your links'), 'warning');
  else drupal_set_message(t('If you had made changes at content type\'s checkboxes settings, you should run the ' . l(t('batch'), 'admin/settings/social-score/synchronization') . ' to synchronize your links'), 'warning');
}