<?php

// $Id: social_score.inc, $

/**
 * @file
 * Provides the social score object and associated methods.
 */

/**
 * Class social_score_score_data
 */
class social_score_score_data {
  public $simple_url = '';
  public $alias_url = '';
  public $score = 0;
  private $current_node = NULL;

  /**
   * Constructor
   */
  public function __construct(&$node) {

    $this->current_node = $node;

    global $base_url;
    $url_category = variable_get('alias', 'noalias');

    if ($url_category === 'noalias') {
      $this->simple_url = $base_url . '/node/' . $node->nid;
    }
    elseif ($url_category === 'alias') {
      $this->alias_url = $base_url . '/' . $node->path;
    }
    else {
      $this->simple_url = $base_url . '/node/' . $nid;
      $this->alias_url = $base_url . '/' . $node->path;
    }
  }

  /**
   * Delete node statistics into database
   */
  public function delete_score() {
    db_query('DELETE FROM {social_score} WHERE nid = %d', $this->current_node->nid);
    return (db_affected_rows() == 0) ? FALSE : TRUE;
  }

  /**
   * Insert score for a node into the social_score table
   */
  public function add_score(&$urls) {
    return $this->get_urls_for_request(&$urls);
  }

  /*
   * Update node statistics into the social_score table
   */
  public function update_score(&$urls) {
    return $this->get_urls_for_request(&$urls);
  }

  /*
   * Get the current urls for a request. Private use only
   */
  public function get_urls_for_request(&$urls) {
    if ($this->simple_url) $urls[] = $this->simple_url;
    if ($this->alias_url) $urls[] = $this->alias_url;
    return $urls;
  }

  /**
   * Extract node score from social_score table
   */
  public function get_score() {
    $this->score = db_result(db_query('SELECT social_score FROM {social_score} WHERE nid = %d', $this->current_node->nid));
    return ($this->score) ? $this->score : 0;
  }

  /**
   *
   * @params $score_statistics (array)
   *
   * $score_statistics is an array which has as a key a url
   * and as a value an array with field values.
   * $score_statistics likes array(url => array (field => value))
   *
   * @return
   *   Returns an array which is an instance of the social score table
   *   $votes = (nid => value, expired => value, social_score => value, timestamp => value, field_1 => value, field_2 => value, ... )
   *
   * Each node might have 2 urls. If there are the urls as keys to $score_statistics,
   * the $votes array will sum both results.
   *
   */
  public function get_node_votes($score_statistics)  {
    $votes = array();
    $votes['nid'] = $this->current_node->nid;
    $votes['expired'] = time()+ variable_get('seconds', 60);
    $votes['social_score'] = 0;
    $votes['timestamp'] = time();

    if ($score_statistics[$this->simple_url])  {
      foreach (array_keys($score_statistics[$this->simple_url]) as $field) {
        $votes[$field] +=  $score_statistics[$this->simple_url][$field];
        if (social_score_is_enabled_field($field)) {
          $votes['social_score'] += $score_statistics[$this->simple_url][$field];
        }
      }
    }
    if ($score_statistics[$this->alias_url])  {
      foreach (array_keys($score_statistics[$this->alias_url]) as $field) {
        $votes[$field] +=  $score_statistics[$this->alias_url][$field];
        if (social_score_is_enabled_field($field)) {
          $votes['social_score'] += $score_statistics[$this->alias_url][$field];
        }
      }
    }
    return $votes;
  }
}